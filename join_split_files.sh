#!/bin/bash

cat product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null >> product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk
rm -f product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null
cat product/app/TrichromeLibrary64/TrichromeLibrary64.apk.* 2>/dev/null >> product/app/TrichromeLibrary64/TrichromeLibrary64.apk
rm -f product/app/TrichromeLibrary64/TrichromeLibrary64.apk.* 2>/dev/null
cat product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> product/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat product/app/Messages/Messages.apk.* 2>/dev/null >> product/app/Messages/Messages.apk
rm -f product/app/Messages/Messages.apk.* 2>/dev/null
cat product/app/WebViewGoogle64/WebViewGoogle64.apk.* 2>/dev/null >> product/app/WebViewGoogle64/WebViewGoogle64.apk
rm -f product/app/WebViewGoogle64/WebViewGoogle64.apk.* 2>/dev/null
cat product/app/YouTube/YouTube.apk.* 2>/dev/null >> product/app/YouTube/YouTube.apk
rm -f product/app/YouTube/YouTube.apk.* 2>/dev/null
cat product/app/Gmail2/Gmail2.apk.* 2>/dev/null >> product/app/Gmail2/Gmail2.apk
rm -f product/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat product/app/MIUINotes/MIUINotes.apk.* 2>/dev/null >> product/app/MIUINotes/MIUINotes.apk
rm -f product/app/MIUINotes/MIUINotes.apk.* 2>/dev/null
cat product/app/Maps/Maps.apk.* 2>/dev/null >> product/app/Maps/Maps.apk
rm -f product/app/Maps/Maps.apk.* 2>/dev/null
cat product/app/MIUIVideoPlayer/MIUIVideoPlayer.apk.* 2>/dev/null >> product/app/MIUIVideoPlayer/MIUIVideoPlayer.apk
rm -f product/app/MIUIVideoPlayer/MIUIVideoPlayer.apk.* 2>/dev/null
cat product/data-app/MIBrowserGlobal_removable/MIBrowserGlobal_removable.apk.* 2>/dev/null >> product/data-app/MIBrowserGlobal_removable/MIBrowserGlobal_removable.apk
rm -f product/data-app/MIBrowserGlobal_removable/MIBrowserGlobal_removable.apk.* 2>/dev/null
cat product/data-app/MISTORE_OVERSEA/MISTORE_OVERSEA.apk.* 2>/dev/null >> product/data-app/MISTORE_OVERSEA/MISTORE_OVERSEA.apk
rm -f product/data-app/MISTORE_OVERSEA/MISTORE_OVERSEA.apk.* 2>/dev/null
cat product/data-app/Photos/Photos.apk.* 2>/dev/null >> product/data-app/Photos/Photos.apk
rm -f product/data-app/Photos/Photos.apk.* 2>/dev/null
cat product/data-app/MIMediaEditorGlobal/MIMediaEditorGlobal.apk.* 2>/dev/null >> product/data-app/MIMediaEditorGlobal/MIMediaEditorGlobal.apk
rm -f product/data-app/MIMediaEditorGlobal/MIMediaEditorGlobal.apk.* 2>/dev/null
cat product/data-app/Duo/Duo.apk.* 2>/dev/null >> product/data-app/Duo/Duo.apk
rm -f product/data-app/Duo/Duo.apk.* 2>/dev/null
cat product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> product/priv-app/Velvet/Velvet.apk
rm -f product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat product/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null >> product/priv-app/MiuiCamera/MiuiCamera.apk
rm -f product/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/priv-app/ExtraPhotoGlobal/ExtraPhotoGlobal.apk.* 2>/dev/null >> product/priv-app/ExtraPhotoGlobal/ExtraPhotoGlobal.apk
rm -f product/priv-app/ExtraPhotoGlobal/ExtraPhotoGlobal.apk.* 2>/dev/null
cat product/priv-app/MIUISecurityCenterGlobal/MIUISecurityCenterGlobal.apk.* 2>/dev/null >> product/priv-app/MIUISecurityCenterGlobal/MIUISecurityCenterGlobal.apk
rm -f product/priv-app/MIUISecurityCenterGlobal/MIUISecurityCenterGlobal.apk.* 2>/dev/null
cat product/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null >> product/priv-app/Phonesky/Phonesky.apk
rm -f product/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null
cat product/priv-app/Gallery_T_Global/Gallery_T_Global.apk.* 2>/dev/null >> product/priv-app/Gallery_T_Global/Gallery_T_Global.apk
rm -f product/priv-app/Gallery_T_Global/Gallery_T_Global.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat vendor_bootimg/10_dtbdump_	qcom,rtic-id.dtb.* 2>/dev/null >> vendor_bootimg/10_dtbdump_	qcom,rtic-id.dtb
rm -f vendor_bootimg/10_dtbdump_	qcom,rtic-id.dtb.* 2>/dev/null
cat cust/app/customized/partner-com.zhiliaoapp.musically_100/partner-com.zhiliaoapp.musically_100.apk.* 2>/dev/null >> cust/app/customized/partner-com.zhiliaoapp.musically_100/partner-com.zhiliaoapp.musically_100.apk
rm -f cust/app/customized/partner-com.zhiliaoapp.musically_100/partner-com.zhiliaoapp.musically_100.apk.* 2>/dev/null
cat cust/app/customized/partner-com.amazon.mp3_123/partner-com.amazon.mp3_123.apk.* 2>/dev/null >> cust/app/customized/partner-com.amazon.mp3_123/partner-com.amazon.mp3_123.apk
rm -f cust/app/customized/partner-com.amazon.mp3_123/partner-com.amazon.mp3_123.apk.* 2>/dev/null
cat cust/app/customized/partner-com.kwai.kuaishou.video.live_155/partner-com.kwai.kuaishou.video.live_155.apk.* 2>/dev/null >> cust/app/customized/partner-com.kwai.kuaishou.video.live_155/partner-com.kwai.kuaishou.video.live_155.apk
rm -f cust/app/customized/partner-com.kwai.kuaishou.video.live_155/partner-com.kwai.kuaishou.video.live_155.apk.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat vendor/camera/mimoji/model2.zip.* 2>/dev/null >> vendor/camera/mimoji/model2.zip
rm -f vendor/camera/mimoji/model2.zip.* 2>/dev/null
cat vendor/lib64/librelight_only.so.* 2>/dev/null >> vendor/lib64/librelight_only.so
rm -f vendor/lib64/librelight_only.so.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk.* 2>/dev/null >> system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk
rm -f system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
